set nocompatible
filetype plugin indent on
let mapleader = ","

" Enable folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |
    \ let python_highlight_all=1

"
" YouCompleteMe config
"
let g:ycm_autoclose_preview_window_after_completion = 1

" Use YouCompleteMe for docs
nnoremap <S-k>  :YcmCompleter GetDoc<CR>

" Set the cursor to the variable to rename, type \yr and then the new name
nnoremap <leader>yr :YcmCompleter RefactorRename

nmap <leader>yh <plug>(YCMHover)

"
" ALE config
"    
"
" Run goimports along gofmt on each save
let g:go_fmt_command = "goimports"

" Automatically get signature/type info for object under cursor
let g:go_auto_type_info = 1           


" ALE Config
let g:ale_linters = {
      \   'python': ['flake8', 'pylint'],
      \}

let g:ale_fixers = {
      \   '*': ['remove_trailing_lines', 'trim_whitespace'],
      \   'python': ['black', 'isort', 'yapf'],
      \}

let g:ale_fix_on_save = 1

syntax on

" Make airline show up
set laststatus=2
set t_Co=256

" Strip comments when joining lines
set fo+=j

set encoding=utf-8

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
