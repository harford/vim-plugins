# vim-plugins

A container for the Vim plugins I use

How to use:

```bash
git clone --recurse-submodules -j8 https://gitlab.com/harford/vim-plugins ~/.vim/pack/${USER}/start
cp ~/.vim/pack/${USER}/start/vimrc ~/.vimrc
```
